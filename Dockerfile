FROM debian:stretch-slim

COPY nvidia-mps /usr/local/bin

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_VOLTA=arch>=7.0

VOLUME /tmp/nvidia-mps

HEALTHCHECK --interval=60s --timeout=1s \
    CMD echo get_server_list | nvidia-cuda-mps-control

CMD ["nvidia-mps"]
